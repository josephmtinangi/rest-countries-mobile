import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Country } from '../../models/country';

import { RestCountries } from '../../providers/rest-countries';

/*
  Generated class for the Countries page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-countries',
  templateUrl: 'countries.html'
})
export class CountriesPage {
  countries: Country[]

  constructor(public navCtrl: NavController, public navParams: NavParams, private restCountries: RestCountries) {
    restCountries.load().subscribe(countries => {
      this.countries = countries;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountriesPage');
  }

}
