import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Country } from '../models/country';

/*
  Generated class for the RestCountries provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class RestCountries {
  restCountriesApiUrl = "https://restcountries-v1.p.mashape.com/all";
  opt: RequestOptions;
  myHeaders: Headers = new Headers;


  constructor(public http: Http) {
    console.log('Hello RestCountries Provider');
    this.myHeaders.set('X-Mashape-Key', 'kAica0sHWemshYVboYcEWw510FR8p1Hc5Pfjsn6BS9ArNYkybu');
    this.myHeaders.append('Accept', 'application/json');

    this.opt = new RequestOptions({
      headers: this.myHeaders
    })
  }

  // Load all countries
  load(): Observable<Country[]> {
    return this.http.get(this.restCountriesApiUrl, this.opt)
      .map(res => <Country[]>res.json());
  }

}
