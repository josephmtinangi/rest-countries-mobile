export interface Country {
    name: string;
    capital: string;
    region: string;
    subregion: string;
    population: number;
}